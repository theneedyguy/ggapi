# GoldenGoblet.net Web Scraper

[![Build Status](https://drone.0x01.host/api/badges/root/theneedyguy-ggapi/status.svg?ref=master)](https://drone.0x01.host/root/theneedyguy-ggapi)

This simple python script provides a static json api by scraping the goldengoblet.net website.

Runs inside a Docker container and outputs the json file internally to /app/output/