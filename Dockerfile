FROM python:3.7-alpine3.8

RUN apk add build-base bash

WORKDIR /app

COPY app.py /app
RUN mkdir -p /app/output
COPY requirements.txt /app
RUN pip install -r requirements.txt

ENTRYPOINT ["bash","-c", "while true; do './app.py'; sleep 3600; done"]