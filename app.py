#!/usr/bin/env python3

import json
from bs4 import BeautifulSoup
import requests
import re

def getLinks():
    # Set base url for links
    base_url = "https://www.goldengoblet.net"
    # Make webrequest to website
    request = requests.get("https://www.goldengoblet.net/games")
    request.encoding = request.apparent_encoding
    s = request.text
    # Init array
    links = []
    soup = BeautifulSoup(s,"html.parser")
    # Get the first item of array since it it only one
    #goblets = soup.find_all("a", {"class": "d-block text-white"})[0]
    # Get the items with the links
    goblet_links = soup.find_all("a", {"class": "d-block text-white"}, href=True)
    # Loop through links
    for goblet in goblet_links:
        print(f'[DEBUG] - {base_url + goblet["href"]}')
        links.append(base_url + goblet["href"])
    return links

def sanitize(input: str):
    return re.sub(' +', ' ', input)

def processLinks(links: list):
    goblet = {"goblets": []}
    for link in links:
        # Make webrequest for link
        request = requests.get(link)
        request.encoding = request.apparent_encoding
        s = request.text

        soup = BeautifulSoup(s,"html.parser")
        # Handle the table logic
        tables = soup.find_all("table", {"class": "results table-center table table-dark"})[0]
        rows = tables.find_all("tr")
        headers = {}
        thead = tables.find("thead")
        if thead:
            thead = thead.find_all("th")
            for i in range(1, len(thead)):
                headers[i-1] = re.sub(' +', ' ', thead[i].text.replace("\n", "").lower().strip())
        print(f'{headers}')
        data = []
        for row in rows:
            cells = row.find_all("td")
            if not cells:
                continue
            if thead:
                items = {}
                for index in headers:
                    # Get placement by class attribute
                    placement = None
                    if cells[index].find("a").find("span", {"class": "first"}):
                        placement = "first"
                    elif cells[index].find("a").find("span", {"class": "second"}):
                        placement = "second"
                    elif cells[index].find("a").find("span", {"class": "third"}):
                        placement = "third"
                    else:
                        placement = None
                    # Create items dictionary
                    items[headers[index]] = {
                        "vod": cells[index].find("a",href=True)['href'],
                        "result": sanitize(cells[index].find("a").find("span", {"class": "comment"}).text.replace("\n","")),
                        "placement": placement
                    }
            # else:
            #     items = []
            #     for index in cells:
            #         items.append(index.text.strip())

            # Append 
            data.append(items)
        
        # Handle rest of the page
        # Get goblet title
        title = soup.find("a", {"id": "gamesDropdown"}).text.strip()
        # Get goblet image
        img = soup.find("img", {"class": "img-fluid"})['src']
        if img:
            img = img
        else:
            img = None
        # Get goblet footnote
        footnote = soup.find("div", {"id": "footnote"})
        if footnote:
            footnote = sanitize(footnote.text.strip().replace("\t","").replace("\n\n","\n"))
        else:
            footnote = None
        print(f'{title}-{data}')

        # Create dictionary
        entryJson = {"title" : title,
        "img": img,
        "footnote" : footnote,
        "days" : data}
        # Append dictionary to goblet dictionary
        goblet['goblets'].append(entryJson)
    # Return the goblet object
    return goblet

# Output to json
with open("./output/goblets.json", "w", encoding="utf-8") as writeJSON:
    # Create a json dump
    json.dump(processLinks(getLinks()), writeJSON, indent=4, sort_keys=True)
writeJSON.close()